package br.edu.ifsc.sorteio;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import android.view.*;
import java.util.Random;

public class MainActivity extends AppCompatActivity {
    private  TextView textoResultado;
    private  EditText editTextMenor;
    private  EditText editTextMaior;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.editTextMenor =  (EditText) findViewById(R.id.editTextMenor);
        this.editTextMaior =  (EditText) findViewById(R.id.editTextMaior);
        this.textoResultado =  (TextView) findViewById(R.id.textoResultado);
    }

    public void sortearNumero( View view ){
        int x = new Random().nextInt( Integer.parseInt (this.editTextMaior.getText().toString())-Integer.parseInt (this.editTextMenor.getText().toString()) )+Integer.parseInt (this.editTextMenor.getText().toString());
        this.textoResultado.setText("O número selecionado é:"+ x);


    }
}
