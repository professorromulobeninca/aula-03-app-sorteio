#Aula 03 -  Sorteio 
Professor Ms Romulo Beninca
e-mail: romulo.beninca@ifsc.edu.br



##Sumário
    Estrutura de um projeto Android.
    Definição de elemento no layout.
    Chamada de método a partir de um evento
    Definição de identificadores.
    Como atribuir e recuperar valores de elementos da view.
    Aplicativo Muda Texto.
    Desenvolvimento do SorteioApp


### Installation
Você pode instalar o projeto apartir do android studio realizando o checkout
    VCS->Checkout From Version Control->Git
   GitRepositoty:https://bitbucket.org/professorromulobeninca/aula-3-app-sorteio/src/master/
### License
 Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
